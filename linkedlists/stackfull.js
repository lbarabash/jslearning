function Stack() {
    this.top = null;
    this.counter = 0;
}

function StackNode(value, next) {
    this.value = value;
    this.next = next;
}

//push: the function that’s used to add elements into the stack
Stack.prototype.push = function(value) {
    this.top = new StackNode(value, this.top);
    this.counter++;
}

//pop: the function that’s used to remove elements from the stack
Stack.prototype.pop = function() {
  if (this.isEmpty()) {
    return null;
  }
  const oldTop = this.top;
  this.top = this.top.next;
  oldTop.next = null;
  this.counter--;
  return oldTop.value;
}

//print: the function that’s used to print elements from the stack
Stack.prototype.print = function() {
    let result = '';
    let currentNode = this.top;
    while (currentNode) {
      result+= currentNode.value + ', ';
      currentNode = currentNode.next;
    }
    console.log(result);
}

//peek: a function that returns the first value (what’s on top of the stack), but doesn’t remove it
Stack.prototype.peek = function() {
  if (this.isEmpty()) {
    return null;
  }
  return this.top.value;
}

//isEmpty: a function that checks if the stack is empty or not — super helpful when trying to clear all the elements from a stack
Stack.prototype.isEmpty = function() {
  if (!this.top) {
    return true;
  }
  return false;
}

//size: a function that returns the number of elements that are in a stack at any given time
Stack.prototype.size = function() {
  return this.counter;
}

const newStack = new Stack();
newStack.push(1);
newStack.push(2);
newStack.push(3);
console.log(newStack.size());
