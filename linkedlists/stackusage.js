const Stack = require('./stacklogic.js');

//push(value): the function that’s used to add element with the passed value into the stack
//pop(): the function that’s used to remove element from the stack and returns the removed element's value
//print(): the function that’s used to print elements from the stack
//peek(): a function that returns the first value (what’s on top of the stack), but doesn’t remove it
//isEmpty(): a function that checks if the stack is empty or not
//size(): a function that returns the number of elements that are in a stack at any given time

const newStack = new Stack();
newStack.push(1);
newStack.push(2);
newStack.push(3);
console.log(newStack.size());
