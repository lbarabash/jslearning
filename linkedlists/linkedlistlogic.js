function LinkedList() {
    this.head = null;
    this.tail = null;
}

function Node(value, next, prev) {
    this.value = value;
    this.next = next;
    this.prev = prev;
}

LinkedList.prototype.addToHead = function(value) {
    let newNode = new Node(value, this.head, null);
    if (this.head) {
        this.head.prev = newNode;
    } else {
        this.tail = newNode;
    }
    this.head = newNode;
};

LinkedList.prototype.addToTail = function(value) {
    let newNode = new Node(value, null, this.tail);
    if (this.tail) {
        this.tail.next = newNode;
    } else {
        this.head = newNode;
    }
    this.tail = newNode;
};

LinkedList.prototype.removeHead = function() {
    if (!this.head) {
        return null;
    }
    const val = this.head.value;
    this.head = this.head.next;
    if (this.head) {
        this.head.prev = null;
    } else {
        this.tail = null;
    }
    return val;
};

LinkedList.prototype.removeTail = function() {
    if (!this.tail) {
        return null;
    }
    let val = this.tail.value;
    this.tail = this.tail.prev;
    if (this.tail) {
        this.tail.next = null;
    } else {
        this.head = null;
    }
    return val;
};

LinkedList.prototype.search = function(searchValue) {
    return this.loopUntil(function(currentNode) {
        return currentNode.value === searchValue;
    });
};

LinkedList.prototype.loopUntil = function(breakCondition) {
    let nodeIndex = 0;
    let currentNode = this.head;
    while (currentNode) {
        if (breakCondition(currentNode, nodeIndex)) {
            return currentNode;
        }
        currentNode = currentNode.next;
        nodeIndex++;
    }
    return null;
};

LinkedList.prototype.forEach = function(callback) {
    let currentNode = this.head;
    while (currentNode) {
        callback(currentNode);
        currentNode = currentNode.next;
    }
};

LinkedList.prototype.print = function() {
    let result = '';
    this.forEach(function(currentNode) {
        result += currentNode.value + ', ';
    });
    console.log(result);
};

LinkedList.prototype.toArray = function() {
    const arr = [];
    this.forEach(function(currentNode) {
        arr.push(currentNode.value);
    });
    return arr;
};

LinkedList.prototype.includes = function(value) {
    return Boolean(this.search(value));
};

LinkedList.prototype.filter = function(value) {
    const newLL = new LinkedList();

    function compare(currentValue) {
        return currentValue === value;
    }
    const condition = typeof value === 'function' ? value : compare;
    this.forEach(function(currentNode) {
        if (condition(currentNode.value)) {
            newLL.addToHead(currentNode.value);
        }
    });
    return newLL;
};

LinkedList.prototype.indexOf = function(value) {
    let currentIndex = 0;
    let currentNode = this.head;
    while (currentNode) {
        if (currentNode.value === value) {
            return currentIndex;
        }
        currentNode = currentNode.next;
        currentIndex++;
    }
    return -1;
};

LinkedList.prototype.getNodeAt = function(nodeIndex) {
    let counter = 0;
    let currentNode = this.head;
    while (counter < nodeIndex) {
        counter++;
        if (currentNode === null) return null;
        currentNode = currentNode.next;
    }
    return currentNode;
};

LinkedList.prototype.concat = function(list) {
    const newLL = new LinkedList();
    const copyNodeToNewLL = function(currentNode) {
        newLL.addToTail(currentNode.value);
    };
    this.forEach(copyNodeToNewLL);
    list.forEach(copyNodeToNewLL);
    return newLL;
};

LinkedList.prototype.append = function(list) {
    this.tail.next = list.head;
    list.head.prev = this.tail;
    this.tail = list.tail;
    return this;
};

LinkedList.prototype.insertAt = function(index, value) {
    if (index === 0) {
        this.addToHead(value);
        return this;
    }
    const node = this.loopUntil(function(currentNode, nodeIndex) {
        return nodeIndex === index;
    });
    if (!node) {
        throw new Error('There is no Node at that index');
    }
    const newNode = new Node(value, null, null);
    const prevNode = node.prev;
    node.prev = newNode;
    prevNode.next = newNode;
    newNode.next = node;
    newNode.prev = prevNode;
    return this;
};

LinkedList.prototype.peek = function() {
    if (!this.head) {
        return null;
    }
    return this.head.value;
};

LinkedList.prototype.isEmpty = function() {
    if (!this.head) {
        return true;
    }
    return false;
};

module.exports = LinkedList;
