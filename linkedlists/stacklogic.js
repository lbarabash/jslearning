const LinkedList = require('./linkedlistlogic.js');

function Stack() {
    this.linkedList = new LinkedList();
}

//push: the function that’s used to add elements into the stack
Stack.prototype.push = function(value) {
    this.linkedList.addToHead(value);
}

//pop: the function that’s used to remove elements from the stack
Stack.prototype.pop = function() {
  return this.linkedList.removeHead();
}

//print: the function that’s used to print elements from the stack
Stack.prototype.print = function() {
  this.linkedList.print();
}

//peek: a function that returns the first value (what’s on top of the stack), but doesn’t remove it
Stack.prototype.peek = function() {
  return this.linkedList.peek();
}

//isEmpty: a function that checks if the stack is empty or not — super helpful when trying to clear all the elements from a stack
Stack.prototype.isEmpty = function() {
  return this.linkedList.isEmpty();
}

//size: a function that returns the number of elements that are in a stack at any given time
Stack.prototype.size = function() {
  let result = 0;
  this.linkedList.forEach(function(currentNode) {
      result++;
  })
  return result;
}

module.exports = Stack;
