const Queue = require('./queuelogic.js');

//enqueue(): the function that’s used to add element with the passed value to the front of Queue
//dequeue(): the function that’s used to remove element from the back of Queue and returns the removed element's value
//peek(): a function that returns the first value (what’s on front of the Queue), but doesn’t remove it
//isEmpty(): a function that checks if the Queue is empty or not
//size(): a function that returns the number of elements that are in a Queue at any given time
//print(): the function that’s used to print the elements from the Queue


const newQueue = new Queue();
newQueue.enqueue(3);
newQueue.enqueue(5);
newQueue.enqueue(4);
newQueue.enqueue(1);
console.log(newQueue.dequeue());
