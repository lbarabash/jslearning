const LinkedList = require('./linkedlistlogic.js');

function Queue() {
    this.linkedList = new LinkedList();
}

//enqueue() : Adds a node (value)
Queue.prototype.enqueue = function(value) {
    this.linkedList.addToHead(value);
}

//dequeue() : Removes and returns the next node in the queue
Queue.prototype.dequeue = function() {
    return this.linkedList.removeHead();
}

//peek() : Returns the value of the node at the front of the queue (without removing)
Queue.prototype.peek = function() {
    return this.linkedList.peek();
}

//isEmpty() : Returns True if the queue is empty, otherwise returns false
Queue.prototype.isEmpty = function() {
    return this.linkedList.isEmpty();
}

//printStack: the function that’s used to print elements from the stack
Queue.prototype.print = function() {
    this.linkedList.print();
}

module.exports = Queue;
