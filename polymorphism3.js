class Employee {
    constructor(name, salary) {
        this.name = name;
        this.salary = salary;
    }

    getSalary() {
        return this.salary;
    }
}

class Manager extends Employee {
    constructor(name, salary, bonus) {
        super(name, salary);
        this.bonus = bonus;
    }

    getSalary() {
        return super.getSalary() + super.getSalary() * this.bonus;
    }
}


const employee1 = new Employee('John', 100000);
const employee2 = new Employee('Jack', 100000);
const employee3 = new Employee('John2', 100000);
const employee4 = new Employee('John3', 100000);
const manager = new Manager('John4', 100000, 0.05);

const employees = [
    employee1,
    employee2,
    employee3,
    employee4,
    manager
];

let sum = 0;
employees.forEach((elem) => {
    sum += elem.getSalary();
});

console.log(sum);
