function BST(value) {
    this.value = value;
    this.left = null;
    this.right = null;
}

BST.raiseErrorForValueUndefined = function(value) {
  if (value === undefined) {
    throw new Error('Value is not defined');
  }
};

BST.prototype.insert = function(value) {
    BST.raiseErrorForValueUndefined(value);
    const childNode = (value <= this.value) ? 'left' : 'right';
    if(this[childNode]) {
      this[childNode].insert(value);
    } else {
      this[childNode] = new BST(value);
    };
    return this;
};

BST.prototype.contains = function(value) {
    BST.raiseErrorForValueUndefined(value);

    if (value === this.value) return true;

    const childNode = (value < this.value) ? 'left' : 'right';
    return (!this[childNode]) ? false : this[childNode].contains(value);
};

BST.prototype.depthFirstTraversalPreOrder = function(iteratorFunc) {
    iteratorFunc(this.value);
    if (this.left) {
        this.left.depthFirstTraversalPreOrder(iteratorFunc);
    }
    if (this.right) {
        this.right.depthFirstTraversalPreOrder(iteratorFunc);
    }
};

BST.prototype.depthFirstTraversalInOrder = function(iteratorFunc) {
    if (this.left) {
        this.left.depthFirstTraversalInOrder(iteratorFunc);
    }
    iteratorFunc(this.value);
    if (this.right) {
        this.right.depthFirstTraversalInOrder(iteratorFunc);
    }
};

BST.prototype.depthFirstTraversalPostOrder = function(iteratorFunc) {
    if (this.left) {
        this.left.depthFirstTraversalPostOrder(iteratorFunc);
    }
    if (this.right) {
        this.right.depthFirstTraversalPostOrder(iteratorFunc);
    }
    iteratorFunc(this.value);
};

BST.prototype.breadthFirstTraversal = function(iteratorFunc) {
    let queue = [this];
    while (queue.length) {
        let treeNode = queue.shift();
        iteratorFunc(treeNode);
        if (treeNode.left) {
            queue.push(treeNode.left);
        }
        if (treeNode.right) {
            queue.push(treeNode.right);
        }
    }
};

BST.prototype.getMinVal = function() {
    return (this.left) ? this.left.getMinVal() : this.value;
};

BST.prototype.getMaxVal = function() {
    return (this.right) ? this.right.getMinVal() : this.value;
};

BST.prototype.size = function() {
    return BST.size(this);
};

BST.size = function(node) {
    if (node === null) return 0;
    return 1 + BST.size(node.left) + BST.size(node.right);
};

BST.prototype.maxDepth = function() {
    return BST.maxDepth(this);
};

BST.maxDepth = function(node) {
    if (node === null) return 0;
    const left = BST.maxDepth(node.left);
    const right = BST.maxDepth(node.right);
    return Math.max(left, right) + 1;
}

BST.prototype.isBalanced = function() {
    const MAX_DEPTH = 2;
    const depthDiff = Math.abs(this.maxDepth(this.left) - this.maxDepth(this.right));
    return depthDiff < MAX_DEPTH;
};

BST.prototype.toBalancedTree = function() {
    const sortedArr = [];
    this.depthFirstTraversalInOrder(function(value) {
        sortedArr.push(value);
    });

    function helper(arr, start, end) {
        if (start > end) return null;

        const median = Math.floor((start + end) / 2);
        const newBst = new BST(arr[median]);
        newBst.left = helper(arr, start, median - 1);
        newBst.right = helper(arr, median + 1, end);
        return newBst;
    };

    return helper(sortedArr, 0, sortedArr.length - 1);
};

BST.prototype.hasNoChildren = function() {
  return this.left === null && this.right === null;
}

BST.prototype.deleteNode = function(value) {
    BST.raiseErrorForValueUndefined(value);

    if (value === this.value) {
      if (this.hasNoChildren()) return null;

      if (this.left === null) return this.right;
      if (this.right === null) return this.left;

      const minVal = this.right.getMinVal();
      this.value = minVal;
      this.right.deleteNode(minVal);
    } else {
      const childNode = (value < this.value) ? 'left' : 'right';
      this[childNode] = this[childNode].deleteNode(value);
    }
    return this;
}

module.exports = BST;
