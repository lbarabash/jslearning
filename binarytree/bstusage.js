const BST = require('./bst.js');

let bst = new BST(12);
bst.insert(7);
bst.insert(15);
bst.insert(14);
bst.insert(20);
bst.insert(2);
bst.insert(5);
bst.insert(3);
bst.insert(8);

const bst2 = bst.toBalancedTree();
bst2.depthFirstTraversalPreOrder(function (value) {
  console.log(value);
})
