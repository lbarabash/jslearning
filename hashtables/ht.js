function HashTable(size) {
    this.buckets = Array(size);
    this.numBuckets = this.buckets.length;
};

function HashNode(key, value, next) {
  this.key = key;
  this.value = value;
  this.next = next || null;
};

HashTable.prototype.hash = function (key) {
  const total = key.split('').reduce(function (sum, element) {
      sum += element.charCodeAt(0);
      return sum;
  }, 0);
  return total % this.numBuckets;
};


HashTable.prototype.forEach = function(callback, index) {
    let currentNode = this.buckets[index];
    while (currentNode) {
        callback(currentNode);
        currentNode = currentNode.next;
    }
};

HashTable.prototype.insert = function (key, value) {
  const index = this.hash(key);
  let firstHashNode = this.firstHashNodeAt(index);

  if (!firstHashNode) {
    this.buckets[index] = new HashNode(key, value);
  }
  else {
    let currentNode = firstHashNode;
    while (currentNode.next) {
      if (currentNode.next.key === key) {
        throw new Error('This key is already exist');
      }
      currentNode = currentNode.next;
    }
    currentNode.next = new HashNode(key, value);
  }
  return this;
};

HashTable.prototype.replace = function(key, value) {
    const index = this.hash(key);
    let firstHashNode = this.firstHashNodeAt(index);

    if (!firstHashNode) throw new Error('Key is not exist');

    if (firstHashNode.key === key) {
        firstHashNode.value = value;
    } else {
        let currentNode = firstHashNode;
        while (currentNode.next) {
            if (currentNode.next.key === key) {
                currentNode.next.value = value;
                return this;
            }
            currentNode = currentNode.next;
        }
    }
    return this;
};


HashTable.prototype.insertOrReplace = function(key, value) {
  const index = this.hash(key);

  if (!this.buckets[index]) {
    this.buckets[index] = new HashNode(key, value);
    return this;
  }

  if (this.buckets[index].key === key) {
    this.buckets[index].value = value;
  }
  else {
    let currentNode = this.buckets[index];
    while (currentNode.next) {
      if (currentNode.next.key === key) {
        currentNode.next.value = value;
        return this;
      }
      currentNode = currentNode.next;
    }
    currentNode.next = new HashNode(key, value);
  }
  return this;
};

HashTable.prototype.get = function (key) {
  const index = this.hash(key);

  if (!this.buckets[index]) { return null }
  else {
    let currentNode = this.buckets[index];
    while (currentNode) {
        if (currentNode.key === key) return currentNode.value;
        currentNode = currentNode.next;
    }
    return null;
  }
};

HashTable.prototype.retrieveAll = function () {
    let allNodes = [];
    for (let i = 0; i < this.numBuckets; i++) {
      this.forEach(function (currentNode) {
        allNodes.push(currentNode);
      }, i)
    }
      return allNodes;
};

HashTable.prototype.firstHashNodeAt = function(index) { return this.buckets[index]; }

HashTable.prototype.delete = function (key) {
  const index = this.hash(key);
  let firstHashNode = this.firstHashNodeAt(index);

  if (!firstHashNode) return null;

  if (firstHashNode.key === key) {
      this.buckets[index] = this.buckets[index].next;
  }  else {
    let currentNode = this.buckets[index];
    let prevNode = null;
    while (currentNode) {
      if (currentNode.key === key) {
        prevNode.next = currentNode.next;
        return this;
      }
      prevNode = currentNode;
      currentNode = currentNode.next;
    }
  }
  return this;
}

module.exports = HashTable;
