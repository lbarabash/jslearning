const HashTable = require('./ht.js');

const myHT = new HashTable(30);
myHT.insert('Dean', 'dean@gmail.com');
myHT.insert('Tom', 'tommy@gmail.com');
myHT.insert('Megan', 'megan@gmail.com');
