// function Shape() {};
//
// Shape.prototype.draw = function () {
//   return "i am generic shape";
// }
//
// Shape.prototype.sayHello = function () {
//   return 'Hello';
// }
//
// //circle
// function Circle() {};
// Circle.prototype = Object.create(Shape.prototype);
// Circle.prototype.draw = function () {
//   return "i am a circle";
// };
//
// //triangle
// function Triangle() {};
// Triangle.prototype = Object.create(Shape.prototype);
// Triangle.prototype.draw = function () {
//   return "this is triangle";
// }
//
// const circle = new Circle();
// const triangle = new Triangle();
// const shape = new Shape();
//
// console.log(circle.draw());

class Shape {
  constructor() {}
  draw() {
    return "i am generic shape";
  }
}

class Circle extends Shape {
  draw() {
    return "i am a circle";
  }
}

class Triangle extends Shape {
  draw() {
    return "this is triangle";
  }
}

const circle = new Circle();
const triangle = new Triangle();
const shape = new Shape();

// console.log(circle.draw());


const shapes = [circle, triangle, shape];
for (let i = 0; i < shapes.length; i++) {
    console.log(
        shapes[i].draw()
    );
}
