// create a new array with only the countries that have a population higher than 500 million.

let data = [
  {
    country: 'China',
    population: 1409517397,
  },
  {
    country: 'India',
    population: 1339180127,
  },
  {
    country: 'USA',
    population: 324459463,
  },
  {
    country: 'Indonesia',
    population: 263991379,
  }
]

const result = data.filter(function (val) {
  return val.population > 500000000;
})

console.log(result);
