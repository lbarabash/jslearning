//multiply arr by 2 with while, for, forEach, map

let arr = [1, 2, 3, 1, 2, 3];

//while
let result1 = [];
let counter = 0;
while (counter < arr.length) {
  result1.push(arr[counter] * 2);
  counter++;
}
//console.log(result1);

//for
let result2 = [];
for (let i = 0; i < arr.length; i++) {
  result2.push(arr[i] * 2);
}
//console.log(result2);

//forEach
let result3 = [];
const arr2 = arr.forEach(val => result3.push(val * 2));
console.log(result3);

//map
const result4 = arr.map(val => val * 2);
//console.log(result4);
