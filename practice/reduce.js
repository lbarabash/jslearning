const arr = [1, 8, 1, 2, 6];

const sum = arr.reduce(function (acc, element) {
    return acc + element;
})

//console.log(sum); // 18

//Using the reduce() method, how would you sum up the population of every country except China?
let data = [
  {
    country: 'China',
    pop: 1409517397,
  },
  {
    country: 'India',
    pop: 1339180127,
  },
  {
    country: 'USA',
    pop: 324459463,
  },
  {
    country: 'Indonesia',
    pop: 263991379,
  }
]

const sumPopulationWithoutChina = data.reduce(function (sum, val,) {
  if (val.country == 'China') {
    return sum;
  }
  return sum + val.pop;
}, 0);

console.log(sumPopulationWithoutChina);
