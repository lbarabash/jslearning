const { expect } = require('chai');
const LinkedList = require('../linkedlists/linkedlistlogic.js');

describe('LinkedList', function() {
    context('when adding an element', function() {
      context('to the head of', function() {
        context('empty list', function() {
          // precondition
          const list = new LinkedList();
          const element = 1;

          // action
          list.addToHead(element);

          it('should have this element as head', function() {
            expect(list.head.value).to.eql(element);
          });

          it('should have this element as tail', function() {
            expect(list.tail.value).to.eql(element);
          });

          // TODO: maybe checking the size is one
          // DONE
          it('should have one element in the list', function() {
            expect(list.head.next).to.eql(null);
          });
        });

        context('list with one element', function() {
            // precondition
            // TODO: read about before / after hooks (https://mochajs.org/#hooks)
            const list = new LinkedList();
            const element = 1;
            const newElement = 2;
            list.addToHead(element);
            const oldHead = list.head;

            // action
            list.addToHead(newElement);

            it('should have newElement as head', function() {
              expect(list.head.value).to.eql(newElement);
            });

            it('should have the old head as tail', function() {
              expect(list.tail).to.eql(oldHead);
            });

            // TODO: maybe checking the size is two
            // DONE
            it('should be two elements in the list', function() {
              expect(list.head.next.next).to.eql(null);
            });
        });

        context('list with two elements', function() {
            // precondition
            const list = new LinkedList();
            const element1 = 1;
            const element2 = 2;
            const newElement = 3;
            list.addToHead(element1);
            list.addToHead(element2);
            const oldTail = list.tail;

            // action
            list.addToHead(newElement);
            const newHead = list.head;

            it('should have a newElement as a head', function() {
              expect(newHead.value).to.eql(newElement);
            });
            it('should not change the tail', function() {
              expect(list.tail).to.eql(oldTail);
            });
            it('should have three elements in the list', function() {
              expect(newHead.next.next.next).to.eql(null);
            });
        });
      });
      context('to the tail of', function() {
          context('empty list', function() {
            // precondition
            const list = new LinkedList();
            const element = 1;

            // action
            list.addToTail(element);

            it('should have this element as tail', function() {
              expect(list.tail.value).to.eql(element);
            });

            it('should have this element as head', function() {
              expect(list.head.value).to.eql(element);
            });

            it('should have one element in the list', function() {
              expect(list.head.next).to.eql(null);
            });
          });

          context('list with one element', function() {
            // precondition
            const list = new LinkedList();
            const element = 1;
            const newElement = 2;
            list.addToTail(element);
            const oldTail = list.tail;

            // action
            list.addToTail(newElement);

            it('should have newElement as tail', function() {
              expect(list.tail.value).to.eql(newElement);
            });

            it('should have an old tail as head', function() {
              expect(list.head.value).to.eql(oldTail.value);
            });

            it('should have two elements in the list', function() {
              expect(list.head.next.next).to.eql(null);
            });
          });

          // TODO: do we have something missing?
          // DONE
          context('list with two elements', function() {
              // precondition
              const list = new LinkedList();
              const element1 = 1;
              const element2 = 2;
              const newElement = 3;
              list.addToTail(element1);
              list.addToTail(element2);
              const oldHead = list.head;

              // action
              list.addToTail(newElement);
              const newHead = list.head;

              it('should have a newElement as a tail', function() {
                expect(list.tail.value).to.eql(newElement);
              });
              it('should not change the head', function() {
                expect(newHead).to.eql(oldHead);
              });
              it('should have three elements in the list', function() {
                expect(newHead.next.next.next).to.eql(null);
              });
          });
      });
    });

    context('when removing an element from the head', function() {
        context('empty list', function() {
            const list = new LinkedList();
            const result = list.removeHead();

            it('should return null', function() {
                expect(result).to.be.eql(null);
            });
        });

        context('list with one element', function() {
            const list = new LinkedList();
            const element = 1;
            list.addToHead(element);
            const result = list.removeHead();

            it('should return the removed head value', function() {
                expect(result).to.be.eql(element);
            });

            it('should be an empty list', function() {
                expect(list.head).to.be.eql(null);
            });
        });

        context('list with two elements', function() {
            // precondition
            const list = new LinkedList();
            const element1 = 1;
            const element2 = 2;
            list.addToHead(element1);
            list.addToHead(element2);
            // action
            const result = list.removeHead();

            it('should return the removed head value', function() {
                expect(result).to.be.eql(element2);
            });

            it('should be a new head', function() {
                expect(list.head.value).to.be.eql(element1);
            });

            // TODO: should we check the size?
            it('should be one element in the list', function() {
              expect(list.head.next).to.eql(null);
            });
        });
    });

    context('when removing an element from the tail', function() {
        context('empty list', function() {
            const list = new LinkedList();

            list.removeTail();

            it('should return null', function() {
                expect(list.tail).to.be.eql(null);
            });
        });

        context('list with one element', function() {
            const list = new LinkedList();
            const element = 1;
            list.addToHead(element);
            // action
            const result = list.removeTail();

            it('should return the removed tail value', function() {
                expect(result).to.be.eql(element);
            });

            it('should be an empty list', function() {
                expect(list.head).to.be.eql(null);
            });
        });

        context('list with two elements', function() {
            const list = new LinkedList();
            const element1 = 1;
            const element2 = 2;
            list.addToHead(element1);
            list.addToHead(element2);
            // action
            const result = list.removeTail();

            it('should return the removed tail value', function() {
                expect(result).to.be.eql(element1);
            });

            it('should be a new tail', function() {
                expect(list.tail.value).to.be.eql(element2);
            });

            it('should be one element in the list', function() {
              expect(list.head.next).to.eql(null);
            });
        });
    });

    context('when searching an element', function() {
        const list = new LinkedList();
        const element = 1;
        const element2 = 2;
        list.addToHead(element);

        it('should return searched element', function() {
            expect(list.search(element).value).to.be.eql(element);
        });

        it('should return null if there is no such element in a list', function() {
            expect(list.search(element2)).to.be.eql(null);
        });
    });

    context('when calling #loopUntil', function() {
      const list = new LinkedList();
      list.addToHead(1);
      list.addToHead(2);
      list.addToHead(3);

      it('should return the element that matches with the break condition', function() {
        const breakCondition = (element) => element.value > 2;
        const node = list.loopUntil(breakCondition);
        expect(node.value).to.eql(3);
      });

      it('should return null if there is no matches with the break condition', function() {
        const breakCondition = function(element) {
          return element.value > 3;
        };
        expect(list.loopUntil(breakCondition)).to.eql(null);
      });
    });

    context('when calling #toArray', function() {
        const list = new LinkedList();
        list.addToHead(1);
        list.addToHead(2);
        list.addToHead(3);
        const listArray = list.toArray();

        it('should return an array of the elements from a list', function() {
          expect(listArray).to.eql([3, 2, 1]);
        });
    });

    context('#includes', () => {
        const list = new LinkedList();
        const element = 4;
        list.addToHead(1);
        list.addToHead(2);
        list.addToHead(3);
        list.addToHead(element);

        context('when the element is present in the list', function() {
          it('should return true', function() {
            expect(list.includes(element)).to.eql(true);
          });
        });

        context('when the element is not present in the list', function() {
          it('should return false', function() {
            expect(list.includes('element')).to.eql(false);
          });
        });
    });

    context('#filter', function() {
      const list = new LinkedList();
      const element = 4;
      list.addToHead(1);
      list.addToHead(2);
      list.addToHead(3);
      list.addToHead(element);
      list.addToHead(element);

      it('should return a new list with filtered element', function() {
        expect(list.filter(element).toArray()).to.eql([4, 4]);
      });

      it('should return an epmty list ', function() {
        expect(list.filter(5).head).to.eql(null);
      });
    });

    context('when searching the index of an element in a list', function() {
      const list = new LinkedList();
      const element = 4;
      const element2 = 5;
      list.addToHead(1);
      list.addToHead(2);
      list.addToHead(3);
      list.addToHead(element);

      it('should return the index of this element in a list', function() {
        expect(list.indexOf(element)).to.eql(0);
      });

      it('should return -1 if this element is not in a list', function() {
        expect(list.indexOf(element2)).to.eql(-1);
      });
    });

    context('when searching an element at the specific element', function() {
      const list = new LinkedList();
      list.addToHead(1);
      list.addToHead(2);
      list.addToHead(3);
      list.addToHead(4);

      it('should return this element', function() {
        expect(list.getNodeAt(0).value).to.eql(4);
      });

      it('should return null if an element is not exist', function() {
        expect(list.getNodeAt(5)).to.eql(null);
      });
    });

    context('when combining 2 lists', function() {
      const list = new LinkedList();
      list.addToHead(1);
      list.addToHead(2);
      list.addToHead(3);
      const list2 = new LinkedList();
      list2.addToHead(1);
      list2.addToHead(2);
      list2.addToHead(3);

      it('should return a new list that contains of list and list2', function() {
        expect(list.concat(list2).toArray()).to.eql([3, 2, 1, 3, 2, 1]);
      });
    });

    context('when adding one lits to another', function() {
        // TODO: how to dry this? (https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)
        // DONE
        function createList() {
          const list = new LinkedList();
          list.addToHead(1);
          list.addToHead(2);
          list.addToHead(3);
          return list;
        };
        const list = createList();
        const list2 = createList();

        // action
        const updatedList = list.append(list2);

      it('should return an updated list', function() {
        expect(updatedList).to.eql(list);
      });

      // TODO: something missing here! Are you sure that the second list is appended?
      // DONE
      it('should be the same heads in the list an in the updated list', function() {
        expect(updatedList.head).to.eql(list.head);
      });
    });

    context('when inserting a new element at the specific index', function() {
        const list = new LinkedList();
        list.addToHead(1);
        list.addToHead(2);
        list.addToHead(3);
        // action
        const result = list.insertAt(1, 25);

        it('should return an updated list with a new element inside', function() {
            expect(result.toArray()).to.eql([3, 25, 2, 1]);
        });

        it('should show an error if the index is not exist', function() {
            const insertWrongIndex = () => list.insertAt(8, 25);
            expect(insertWrongIndex).to.throw('There is no Node at that index');
        });
    });

    context('when checking an element of a head', function() {
        const list = new LinkedList();
        list.addToHead(1);

        it('should return the value of a head from the list', function() {
            expect(list.peek()).to.eql(list.head.value);
        });

        it('should return null if the list is empty', function() {
            list.removeHead();
            expect(list.peek()).to.eql(null);
        });
    });

    context('when checking if a list is empty', function() {
        let list = null;
        beforeEach(function() {
          list = new LinkedList();
        });

        it('should return false if a list is not empty', function() {
          list.addToHead(1);
          expect(list.isEmpty()).to.eql(false);
        });

        it('should return true if a list is empty', function() {
            expect(list.isEmpty()).to.eql(true);
        });
    });
});
